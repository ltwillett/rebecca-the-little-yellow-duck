Rebecca was a little yellow duck.

Rebecca worked at a bank, had very good taste in shoes and had excellent table manners. Rebecca spoke pleasantly to her neighbours, called her mother every Sunday and went to church regularly. All this was harder for Rebecca than it should have been, though. 

Rebecca lived in a town called Bear Hill, where the local population consisted of ants and bears. This town was wonderfully friendly - the ants were too small to get squished by the bears, and the bears were too friendly to be bothered by the ants. This difference in size however, was not very nice for Rebecca.

As a little yellow duck in this town, Rebecca often found things either too big or too small to fit comfortably. This little yellow duck woke every morning in her bear-sized bed, walked to her ant-sized kitchen and made herself an ant-sized breakfast. Rebecca walked to the bear-sized bus stop, got on the bear-sized bus and sat in an ant-sized seat. 

She drank from ant-sized cups at the bank, and served big bear-sized customers. Pushing the heavy bear-sized doors open, Rebecca got out ant-sized money out of the bank's safe. Rebecca would then catch the bear-sized bus in her ant-sized seat home, lie on her bear-sized couch and fall into a duck-sized sleep, exhausted.

Rebecca found herself dreaming of a little yellow duck-sized pond to swim in and a little yellow duck-sized playground to play in. Rebecca wished for a little yellow duck-sized kitchen, where she could make little yellow duck-sized food and have little yellow duck-sized friends over for parties. She would wear little yellow duck-sized shoes and a little yellow duck-sized dress and she would feel beautiful.

One morning, Rebecca awoke to find everything... slightly different. Her feet touched the end of the bed. Rebecca then rolled over, sat up and her feet touched the ground. Rebecca walked to her kitchen and she could fit in between the bench and the sink. She then made breakfast and felt satisfied. Walking to the bus, Rebecca noticed how much faster she seemed to be walking - she arrived at the bus stop in no time!

She boarded the bus and found a seat that fit her comfortably. As the bus drove past the shops, Rebecca saw shoes and dresses that would fit her! Rebecca started to wonder about all this. 

"Have I grown up?", Rebecca thought. "Have some things gotten bigger and others gotten smaller? What is going on?!?"

Arriving at the bank, Rebecca's hopes were confirmed - the customers weren't big and scary, the bank doors weren't massive and heavy and the money wasn't tiny and fiddly. Everything was the perfect size for a little yellow duck. 

"Wow!", Rebecca exclaimed. "Everything is my size! Think of all the things I can do."